FROM node:lts-buster-slim

WORKDIR /meshcentral
ENV NODE_ENV=production
RUN apt-get -y update
RUN apt-get -y install npm

RUN useradd -ms /bin/bash meshcentral
RUN chown -R meshcentral:meshcentral /meshcentral

USER meshcentral:meshcentral

RUN npm install otplib@10.2.3
RUN npm install semver
RUN npm install meshcentral

EXPOSE 80 443

USER root:root

COPY start.sh /start.sh

CMD ["sh", "/start.sh"]
